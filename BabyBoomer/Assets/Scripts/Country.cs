using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Country : MonoBehaviour
{
    [SerializeField] string countryName;

    Image image;

    bool selected;

    private void Start()
    {
        image = GetComponent<Image>();

        gameObject.name = countryName;

        selected = false;
    }

    public string GetCountryName()
    {
        return countryName;
    }

    public bool IsSelected()
    {
        return selected;
    }

    public void Select()
    {
        if (selected)
        {
            image.color = Color.white;

            selected = false;
        }
        else
        {
            image.color = Color.yellow;
            selected = true;
        }
    }

}
