using System.Collections;
using System.Collections.Generic;
using UnityEditor.Presets;
using UnityEngine;

public class CountryButton : MonoBehaviour, Iinteractable
{
    [SerializeField] string countryName;

    bool pressed = false;   
    public void Interact()
    {
        if(!pressed)
            Press();
    }

    public void Outline()
    {
        
    }

    void Press()
    {
        Vector3 animate = new Vector3(Mathf.Lerp(transform.localPosition.x, -.1f, 1f),transform.localPosition.y, transform.localPosition.z);

        transform.localPosition = animate;

        pressed = true;

        EventManager.Instance.PressCountryButton(countryName);

        StartCoroutine(Release(.5f));
    }

    IEnumerator Release(float pTime)
    {
        float mTime = pTime;

        while (mTime > 0f)
        {
            yield return new WaitForSecondsRealtime(pTime);
            mTime--;
        }

        Vector3 animate = new Vector3(Mathf.Lerp(transform.localPosition.x, .4f, 1f), transform.localPosition.y, transform.localPosition.z);

        transform.localPosition = animate;

        pressed = false;
    }
}
