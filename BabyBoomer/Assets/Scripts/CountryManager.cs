using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountryManager : MonoBehaviour
{
    [SerializeField] List<Country> countryList;

    Dictionary<string, Country> countryDict = new Dictionary<string, Country>();

    Country selectedCountry = null;

    private void Start()
    {
        EventManager.Instance.onCountryButtonPressed += SelectCountry;

        foreach (Country country in countryList)
        {
            countryDict.Add(country.GetCountryName(), country);
        }
    }


    void SelectCountry(string countryName)
    {
        if(selectedCountry != null)
        {
            if(selectedCountry != countryDict[countryName])
            {
                selectedCountry.Select();

                selectedCountry = null;

                selectedCountry = countryDict[countryName];

                selectedCountry.Select();         
            }
        }
        else
        {
            selectedCountry = countryDict[countryName];

            selectedCountry.Select();
        }

       
    }

}
