using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
   public static EventManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public event Action<string> onCountryButtonPressed;
    public event Action<bool> onLeverSwapped;


    public void PressCountryButton(string countryName)
    {
        if (onCountryButtonPressed != null)
            onCountryButtonPressed(countryName);
    }

    public void ChangeChannel(bool up)
    {
        if(onLeverSwapped != null)
            onLeverSwapped(up);
    }

}
