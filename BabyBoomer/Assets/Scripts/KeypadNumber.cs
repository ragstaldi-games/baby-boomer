using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadNumber : MonoBehaviour, Iinteractable
{
    bool pressed = false;
    public void Interact()
    {
        if (!pressed)
            Press();
    }

    public void Outline()
    {
        
    }


    void Press()
    {
        Vector3 animate = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, .45f, 1f), transform.localPosition.z);

        transform.localPosition = animate;

        pressed = true;

        StartCoroutine(Release(.5f));
    }

    IEnumerator Release(float pTime)
    {
        float mTime = pTime;

        while (mTime > 0f)
        {
            yield return new WaitForSecondsRealtime(pTime);
            mTime--;
        }

        Vector3 animate = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, .5f, 1f), transform.localPosition.z);

        transform.localPosition = animate;

        pressed = false;
    }

}
