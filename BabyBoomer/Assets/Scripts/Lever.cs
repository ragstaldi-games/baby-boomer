using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TVChannel
{
    NEWS = 0,
    KIDS = 1,
}

public class Lever : MonoBehaviour, Iinteractable
{
    [SerializeField] GameObject leverSwitch;

    TVChannel channel;

    bool isUp = true;

    public void Interact()
    {
        ToggleSwitch();
    }

    public void Outline()
    {
        
    }


    void ToggleSwitch()
    {
        if (isUp)
        {
            Vector3 animate = new Vector3(leverSwitch.transform.localPosition.x, Mathf.Lerp(leverSwitch.transform.localPosition.y, -.3f, 1f), leverSwitch.transform.localPosition.z);

            leverSwitch.transform.localPosition = animate;

            ChangeChannel(TVChannel.NEWS);

            isUp = false;

        }
        else
        {
            Vector3 animate = new Vector3(leverSwitch.transform.localPosition.x, Mathf.Lerp(leverSwitch.transform.localPosition.y, .3f, 1f), leverSwitch.transform.localPosition.z);

            leverSwitch.transform.localPosition = animate;

            ChangeChannel(TVChannel.KIDS);

            isUp = true;
        }
    }

    private void Start()
    {
        channel = TVChannel.KIDS;
    }


    void ChangeChannel(TVChannel pChannel)
    {
        channel = pChannel;

        if (channel == TVChannel.KIDS)
            EventManager.Instance.ChangeChannel(true);
        else
            EventManager.Instance.ChangeChannel(false);
    }
}
