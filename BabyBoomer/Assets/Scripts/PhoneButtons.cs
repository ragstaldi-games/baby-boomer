using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneButtons : MonoBehaviour, Iinteractable
{

    bool pressed = false;

    public void Interact()
    {
        if (!pressed)
            Press();
    }

    public void Outline()
    {
        
    }

    void Press()
    {
        Vector3 animate = new Vector3(.05f, .05f, .05f);

        transform.localPosition -= animate;

        pressed = true;

        StartCoroutine(Release(.5f));
    }

    IEnumerator Release(float pTime)
    {
        float mTime = pTime;

        while (mTime > 0f)
        {
            yield return new WaitForSecondsRealtime(pTime);
            mTime--;
        }

        Vector3 animate = new Vector3(.05f, .05f, .05f);

        transform.localPosition += animate;

        pressed = false;
    }
}
