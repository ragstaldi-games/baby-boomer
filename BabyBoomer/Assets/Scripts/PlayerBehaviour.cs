using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;

    [SerializeField] Volume postPro;

    Vignette vignette;

    Animator animator;

    Rigidbody rb;

    GameObject mamadera;

    float asleepMeter = 0f;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        postPro.sharedProfile.TryGet<Vignette>(out vignette);

        vignette.intensity.value = 0;

        EventManager.Instance.onLeverSwapped += ChannelChanged;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (movimientoAdelanteAtras != 0)
            animator.SetBool("Idle", false);
        else
            animator.SetBool("Idle", true);


        Ray ray = Camera.main.ViewportPointToRay(new Vector3(.5f, .5f, 0));

        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
        {
            if(hit.collider.gameObject.TryGetComponent<Iinteractable>(out Iinteractable interactable))
            {
                interactable.Outline();

                if(Input.GetMouseButtonDown(0))
                    interactable.Interact();
            }
        }

    }

    void ChannelChanged(bool up)
    {
        if (up)
        {
            StopAllCoroutines();

            vignette.intensity.value = 0;

            asleepMeter = 0;
        }
        else
        {
            StartCoroutine(SleepRoutine());
        }
            
    }


    IEnumerator SleepRoutine()
    {
        while (asleepMeter < 1)
        {
            yield return new WaitForSecondsRealtime(1.0f);

            asleepMeter += 0.01f;

            vignette.intensity.value += 0.01f;
        }
    }

}
