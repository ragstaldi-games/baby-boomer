using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedButton : MonoBehaviour, Iinteractable
{
    [SerializeField] GameObject button;



    bool pressed = false;

    public void Interact()
    {
        if (!pressed)
            Press();
    }

    public void Outline()
    {
        
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void Press()
    {
        pressed = true;

        Vector3 animate =  new Vector3(Mathf.Lerp(button.transform.localPosition.x, .2f, 1f), 0,0);

        button.transform.localPosition = animate;

        StartCoroutine(ReleaseRoutine(.5f));

    }

   IEnumerator ReleaseRoutine(float pTime)
    {
        float mTime = pTime;

        while (mTime > 0f)
        {
            yield return new WaitForSecondsRealtime(pTime);
            mTime--;
        }

        Vector3 animate = new Vector3(Mathf.Lerp(button.transform.localPosition.x, 1f, 1f), 0, 0);

        button.transform.localPosition = animate;

        pressed = false;
    }

}
